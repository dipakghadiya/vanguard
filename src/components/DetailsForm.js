import './DetailsForm.scss';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import FormHelperText from '@mui/material/FormHelperText';
import { useState } from 'react';
import {
  handleChangeWithValidation,
  titles,
  regions,
  loadCountries,
} from '../common/utils';

function DetailsForm() {
  const [title, setTitle] = useState({ value: '', isValid: true });
  const [firstName, setFirstName] = useState({ value: '', isValid: true });
  const [middleName, setMiddleName] = useState({ value: '' });
  const [lastName, setLastName] = useState({ value: '', isValid: true });
  const [email, setEmail] = useState({ value: '', isValid: true });
  const [confirmationEmail, setConfirmationEmail] = useState({
    value: '',
    isValid: true,
  });
  const [insuranceNo, setInsuranceNo] = useState({ value: '', isValid: true });
  const [region, setRegion] = useState({ value: '', isValid: true });
  const [nationality, setNationality] = useState({ value: '', isValid: true });
  const [countries, setCountries] = useState([]);

  const validateConfirmationEmail = () => {
    setConfirmationEmail({
      value: confirmationEmail.value,
      isValid: confirmationEmail.value === email.value,
    });
  };

  const validateForm = () => {
    handleChangeWithValidation(title.value, setTitle, 'title');
    handleChangeWithValidation(firstName.value, setFirstName, 'firstName');
    handleChangeWithValidation(lastName.value, setLastName, 'lastName');
    handleChangeWithValidation(email.value, setEmail, 'email');
    handleChangeWithValidation(
      confirmationEmail.value,
      setConfirmationEmail,
      'confirmationEmail',
      confirmationEmail.value !== email.value
    );
    handleChangeWithValidation(
      insuranceNo.value,
      setInsuranceNo,
      'insuranceNo'
    );
    handleChangeWithValidation(region.value, setRegion, 'region');
    handleChangeWithValidation(
      nationality.value,
      setNationality,
      'nationality'
    );
  };

  return (
    <div className='app-form'>
      <FormControl
        variant='standard'
        className='form-input title'
        required
        error={!title.isValid}
      >
        <InputLabel id='demo-simple-select-standard-label'>Title</InputLabel>
        <Select
          labelId='demo-simple-select-standard-label'
          id='demo-simple-select-standard'
          label='Title'
          value={title.value}
          onChange={(event) =>
            handleChangeWithValidation(event.target.value, setTitle, 'title')
          }
        >
          {titles.map((title) => (
            <MenuItem key={title} value={title}>
              {title}
            </MenuItem>
          ))}
        </Select>
        {!title.isValid && (
          <FormHelperText>Please select the title</FormHelperText>
        )}
      </FormControl>
      <TextField
        className='form-input first-name'
        label='Legal First name'
        variant='standard'
        helperText={
          !firstName.isValid
            ? firstName?.value?.length > 0
              ? 'Please enter only latters'
              : 'Please enter firstname'
            : ''
        }
        error={!firstName.isValid}
        required
        value={firstName.value}
        onChange={(event) =>
          handleChangeWithValidation(
            event.target.value,
            setFirstName,
            'firstName'
          )
        }
      />
      <TextField
        className='form-input middle-name'
        label='Middle name(s)'
        variant='standard'
        value={middleName.value}
        onChange={(event) => setMiddleName({ value: event.target.value })}
      />
      <TextField
        className='form-input last-name'
        label='Last name'
        variant='standard'
        helperText={
          !lastName.isValid
            ? lastName?.value?.length > 0
              ? 'Please enter only latters'
              : 'Please enter lastname'
            : ''
        }
        error={!lastName.isValid}
        required
        value={lastName.value}
        onChange={(event) =>
          handleChangeWithValidation(
            event.target.value,
            setLastName,
            'lastName'
          )
        }
      />
      <TextField
        className='form-input email'
        label='Email'
        variant='standard'
        helperText={!email.isValid ? 'Please enter valid email' : ''}
        error={!email.isValid}
        required
        value={email.value}
        onChange={(event) =>
          handleChangeWithValidation(event.target.value, setEmail, 'email')
        }
        onBlur={() => validateConfirmationEmail()}
      />
      <TextField
        className='form-input confirmation-email'
        label='Confirmation Email'
        variant='standard'
        helperText={
          !confirmationEmail.isValid
            ? confirmationEmail.value !== email.value
              ? 'Confirmation email should match with email'
              : 'Please enter valid confirmation email'
            : ''
        }
        error={!confirmationEmail.isValid}
        required
        value={confirmationEmail.value}
        onChange={(event) =>
          handleChangeWithValidation(
            event.target.value,
            setConfirmationEmail,
            'confirmationEmail',
            event.target.value !== email.value
          )
        }
      />
      <TextField
        className='form-input insurance-no'
        label='National Insurance number'
        variant='standard'
        helperText={
          !insuranceNo.isValid
            ? 'Please enter valid National Insurance number'
            : ''
        }
        error={!insuranceNo.isValid}
        required
        value={insuranceNo.value}
        onChange={(event) =>
          handleChangeWithValidation(
            event.target.value,
            setInsuranceNo,
            'insuranceNo'
          )
        }
      />
      <FormControl
        variant='standard'
        className='form-input region'
        required
        error={!region.isValid}
      >
        <InputLabel id='demo-simple-select-standard-label'>Region</InputLabel>
        <Select
          labelId='demo-simple-select-standard-label'
          id='demo-simple-select-standard'
          label='Region'
          value={region.value}
          onChange={(event) =>
            handleChangeWithValidation(event.target.value, setRegion, 'region')
          }
          onBlur={async () => setCountries(await loadCountries(region.value))}
        >
          {regions.map((value) => (
            <MenuItem value={value} key={value}>
              {value}
            </MenuItem>
          ))}
        </Select>
        {!region.isValid && (
          <FormHelperText>Please select the region</FormHelperText>
        )}
      </FormControl>
      <FormControl
        variant='standard'
        className='form-input nationality'
        error={!nationality.isValid}
        required
      >
        <InputLabel id='demo-simple-select-standard-label'>
          Country of nationality
        </InputLabel>
        <Select
          labelId='demo-simple-select-standard-label'
          id='demo-simple-select-standard'
          label='Country of nationality'
          value={nationality.value}
          disabled={countries.length === 0}
          onChange={(event) =>
            handleChangeWithValidation(
              event.target.value,
              setNationality,
              'nationality'
            )
          }
        >
          {countries.map((country) => (
            <MenuItem key={country.id} value={country.id}>
              {country.name}
            </MenuItem>
          ))}
        </Select>
        {!nationality.isValid && (
          <FormHelperText>
            Please select the country of nationality
          </FormHelperText>
        )}
      </FormControl>
      <Button
        className='next-button'
        variant='outlined'
        onClick={validateForm}
        disabled={
          !(
            title.isValid &&
            firstName.isValid &&
            lastName.isValid &&
            email.isValid &&
            confirmationEmail.isValid &&
            insuranceNo.isValid &&
            region.isValid &&
            nationality.isValid
          )
        }
      >
        Next
      </Button>
    </div>
  );
}

export default DetailsForm;
