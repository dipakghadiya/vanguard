import React from 'react';
import { act, create } from 'react-test-renderer';
import DetailsForm from '../DetailsForm';

describe('DetailsForm component', () => {
  let component;
  let instance;

  beforeEach(() => {
    component = create(<DetailsForm />);
    instance = component.root;
  });

  it('should render correctly', () => {
    expect(component.toJSON()).toMatchSnapshot();
  });

  it('should render error for first name if digits are provided in value', async () => {
    const element = instance.findByProps({ label: 'Legal First name' });

    expect(element.props.error).toBeFalsy();
    expect(element.props.helperText).toBe('');

    await act(async () => {
      element.props.onChange({ target: { value: 'abc123' } });
      await component.update(<DetailsForm />);
    });

    expect(element.props.error).toBeTruthy();
    expect(element.props.helperText).toBe('Please enter only latters');

    expect(component.toJSON()).toMatchSnapshot();
  });

  it('should render error if first name left blank', async () => {
    const element = instance.findByProps({ label: 'Legal First name' });

    expect(element.props.error).toBeFalsy();
    expect(element.props.helperText).toBe('');

    await act(async () => {
      element.props.onChange({ target: { value: '' } });
      await component.update(<DetailsForm />);
    });

    expect(element.props.error).toBeTruthy();
    expect(element.props.helperText).toBe('Please enter firstname');

    expect(component.toJSON()).toMatchSnapshot();
  });

  it('should render error if email is invalid', async () => {
    const element = instance.findByProps({ label: 'Email' });

    expect(element.props.error).toBeFalsy();
    expect(element.props.helperText).toBe('');

    await act(async () => {
      element.props.onChange({ target: { value: 'abc123@asd' } });
      await component.update(<DetailsForm />);
    });

    expect(element.props.error).toBeTruthy();
    expect(element.props.helperText).toBe('Please enter valid email');

    expect(component.toJSON()).toMatchSnapshot();
  });

  it('should render correct if email is valid', async () => {
    const element = instance.findByProps({ label: 'Email' });

    expect(element.props.error).toBeFalsy();
    expect(element.props.helperText).toBe('');

    await act(async () => {
      element.props.onChange({ target: { value: 'abc123@asd.com' } });
      await component.update(<DetailsForm />);
    });

    expect(element.props.error).toBeFalsy();
    expect(element.props.helperText).toBe('');

    expect(component.toJSON()).toMatchSnapshot();
  });

  it('should render error if confirmation email is invalid', async () => {
    const element = instance.findByProps({ label: 'Confirmation Email' });

    expect(element.props.error).toBeFalsy();
    expect(element.props.helperText).toBe('');

    await act(async () => {
      instance
        .findByProps({ label: 'Email' })
        .props.onChange({ target: { value: 'abc123@asd' } });
      element.props.onChange({ target: { value: 'abc123@asd' } });
      await component.update(<DetailsForm />);
    });

    expect(element.props.error).toBeTruthy();
    expect(element.props.helperText).toBe(
      'Please enter valid confirmation email'
    );

    expect(component.toJSON()).toMatchSnapshot();
  });

  it('should render error if confirmation email is not same as email', async () => {
    const element = instance.findByProps({ label: 'Confirmation Email' });

    expect(element.props.error).toBeFalsy();
    expect(element.props.helperText).toBe('');

    await act(async () => {
      instance
        .findByProps({ label: 'Email' })
        .props.onChange({ target: { value: 'abc@asd.com' } });
      element.props.onChange({ target: { value: 'abc123@asd.com' } });
      await component.update(<DetailsForm />);
    });

    expect(element.props.error).toBeTruthy();
    expect(element.props.helperText).toBe(
      'Confirmation email should match with email'
    );

    expect(component.toJSON()).toMatchSnapshot();
  });

  it('should render correct if confirmation email is same as email', async () => {
    const element = instance.findByProps({ label: 'Confirmation Email' });

    expect(element.props.error).toBeFalsy();
    expect(element.props.helperText).toBe('');

    await act(async () => {
      instance
        .findByProps({ label: 'Email' })
        .props.onChange({ target: { value: 'abc@asd.com' } });
      await component.update(<DetailsForm />);
      element.props.onChange({ target: { value: 'abc@asd.com' } });
      await component.update(<DetailsForm />);
    });

    expect(element.props.error).toBeFalsy();
    expect(element.props.helperText).toBe('');

    expect(component.toJSON()).toMatchSnapshot();
  });

  it('should render error if insurance number is not valid', async () => {
    const element = instance.findByProps({
      label: 'National Insurance number',
    });

    expect(element.props.error).toBeFalsy();
    expect(element.props.helperText).toBe('');

    await act(async () => {
      element.props.onChange({ target: { value: '12hbhj12h1b' } });
      await component.update(<DetailsForm />);
    });

    expect(element.props.error).toBeTruthy();
    expect(element.props.helperText).toBe(
      'Please enter valid National Insurance number'
    );

    expect(component.toJSON()).toMatchSnapshot();
  });

  it('should render correctly if insurance number is valid', async () => {
    const element = instance.findByProps({
      label: 'National Insurance number',
    });

    expect(element.props.error).toBeFalsy();
    expect(element.props.helperText).toBe('');

    await act(async () => {
      element.props.onChange({ target: { value: 'AS123456D' } });
      await component.update(<DetailsForm />);
    });

    expect(element.props.error).toBeFalsy();
    expect(element.props.helperText).toBe('');

    expect(component.toJSON()).toMatchSnapshot();
  });

  it('should render Contry dropdown selection disabled if no region is selected', async () => {
    expect(instance.findByProps({ label: 'Region' }).props.value).toBe('');
    expect(
      instance.findByProps({ label: 'Country of nationality' }).props.disabled
    ).toBeTruthy();
  });

  it('should render Contry dropdown selection enabled if region is selected', async () => {
    await act(async () => {
      instance
        .findByProps({ label: 'Region' })
        .props.onChange({ target: { value: 'Asia' } });
      await component.update(<DetailsForm />);

      instance.findByProps({ label: 'Region' }).props.onBlur();
      await component.update(<DetailsForm />);
    });

    expect(
      instance.findByProps({ label: 'Country of nationality' }).props.disabled
    ).toBeFalsy();

    expect(component.toJSON()).toMatchSnapshot();
  });

  it('should render Next button disabled if any input is invalid', async () => {
    const element = instance.findByProps({ label: 'Legal First name' });

    expect(element.props.error).toBeFalsy();
    expect(element.props.helperText).toBe('');

    await act(async () => {
      element.props.onChange({ target: { value: 'abc123' } });
      await component.update(<DetailsForm />);
    });

    expect(element.props.error).toBeTruthy();
    expect(
      instance.findByProps({ className: 'next-button' }).props.disabled
    ).toBeTruthy();

    expect(component.toJSON()).toMatchSnapshot();
  });

  it('should render Next button enabled on load of page', async () => {
    expect(
      instance.findByProps({ className: 'next-button' }).props.disabled
    ).toBeFalsy();
  });

  it('should run validation on click on next button', async () => {
    await act(async () => {
      instance.findByProps({ className: 'next-button' }).props.onClick();
      await component.update(<DetailsForm />);
    });

    expect(
      instance.findByProps({ className: 'form-input title' }).props.error
    ).toBeTruthy();
    expect(
      instance.findByProps({ className: 'form-input first-name' }).props.error
    ).toBeTruthy();
    expect(
      instance.findByProps({ className: 'form-input last-name' }).props.error
    ).toBeTruthy();
    expect(
      instance.findByProps({ className: 'form-input email' }).props.error
    ).toBeTruthy();
    expect(
      instance.findByProps({ className: 'form-input confirmation-email' }).props
        .error
    ).toBeTruthy();
    expect(
      instance.findByProps({ className: 'form-input insurance-no' }).props.error
    ).toBeTruthy();
    expect(
      instance.findByProps({ className: 'form-input region' }).props.error
    ).toBeTruthy();
    expect(
      instance.findByProps({ className: 'form-input nationality' }).props.error
    ).toBeTruthy();
  });
});
