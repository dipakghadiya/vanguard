import React from 'react';
import { act, create } from 'react-test-renderer';
import App from '../App';

describe('App component', () => {
  it('should render correctly', async () => {
    let component;
    await act(async () => {
      component = await create(<App />);
      const tree = component.toJSON();
      expect(tree).toMatchSnapshot();
    });
  });
});
