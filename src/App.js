import logo from './logo.png';
import './App.scss';
import DetailsForm from './components/DetailsForm';

function App() {
  return (
    <div className='app'>
      <header className='app-header'>
        <div className='app-view app-header--logo'>
          <img src={logo} alt='logo' />
        </div>
      </header>
      <section className='app-view app-content'>
        <h1>Please fill in your details to complete the process</h1>
        <DetailsForm />
      </section>
    </div>
  );
}

export default App;
