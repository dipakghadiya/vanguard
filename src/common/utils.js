import data from './data.json';

const IS_REQUIRED = /\S+/;
const ONLY_LATTERS = /^[a-z]+$/i;
const EMAIL_VALIDATION = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
const INSURANCE_NO_VALIDATION = /^[a-zA-Z]{2}\d{6}[a-zA-Z]$/;

const formValidationRegEx = {
  title: [IS_REQUIRED],
  firstName: [IS_REQUIRED, ONLY_LATTERS],
  lastName: [IS_REQUIRED, ONLY_LATTERS],
  email: [IS_REQUIRED, EMAIL_VALIDATION],
  confirmationEmail: [IS_REQUIRED, EMAIL_VALIDATION],
  insuranceNo: [IS_REQUIRED, INSURANCE_NO_VALIDATION],
  region: [IS_REQUIRED],
  nationality: [IS_REQUIRED],
};

const handleChangeWithValidation = (
  value,
  stateHandler,
  parameter,
  isInvalid
) => {
  const isValid = validateField(value, parameter, isInvalid);
  stateHandler({ value, isValid });

  return isValid;
};

const validateField = (value, parameter, isInvalid) => {
  const validations = formValidationRegEx[parameter] || [];
  let isValid = !isInvalid;

  validations.forEach((validation) => {
    isValid = isValid && validation.test(value);
  });

  return isValid;
};

const regions = ['America', 'Asia', 'Africa', 'Oceania', 'Europe'];
const titles = ['Mr.', 'Mrs.', 'Ms.'];

const loadCountries = async (region) => {
  if (!region || region.length === 0) {
    return [];
  }

  return data.filter((country) => country.region.includes(region));
};

export { handleChangeWithValidation, titles, regions, loadCountries };
